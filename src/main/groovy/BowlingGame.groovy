
class BowlingGame {

    class Frame {
        Integer firstRoll = null
        Integer secondRoll = null

        def getFrameScore() { (firstRoll ?: 0) + (secondRoll ?: 0) }
        def getIsStrike() { firstRoll == 10 }
        def getIsSpare() { frameScore == 10 }
    }

    List<Frame> rolls

    def roll(int pinsKnockedDown) {
        if (!rolls) {
            rolls = [new Frame(firstRoll: pinsKnockedDown)]
        } else {
            def lastFrame = rolls.last()
            if (lastFrame.isStrike || lastFrame.secondRoll != null) {
                rolls << new Frame(firstRoll: pinsKnockedDown)
            } else {
                lastFrame.secondRoll = pinsKnockedDown
            }
        }
    }

    def score() {
        def sum = 0
        (0..9).each { i ->
            Frame frame = rolls[i];
            Frame nextFrame = rolls[i + 1]

            if (frame.isStrike) {
                sum += frame.frameScore + nextFrame.frameScore
                if (nextFrame.isStrike) {
                    sum += rolls[i + 2].firstRoll
                }
            } else if (frame.isSpare) {
                sum += frame.frameScore + nextFrame.firstRoll
            } else {
                sum += frame.frameScore
            }
        }
        sum
    }
}