import spock.lang.*

class BowlingGameSpec extends Specification {
    BowlingGame game

    void setup() {
        game = new BowlingGame()
    }

    void "Rolling gutter balls all game should return a score of 0"() {
        when:
            rollMany 20, 0

        then:
            game.score() == 0
    }

    void "Rolling all 1's all game should return a score of 20"() {
        when:
            rollMany 20, 1

        then:
            game.score() == 20
    }

    void "Rolling a spare gets correct score calculated"() {
        when:
            game.roll 1
            game.roll 9
            game.roll 4
            rollMany 17, 0

        then:
            game.score() == 18
    }

    void "Rolling a strike gets correct score calculated"() {
        when:
            game.roll 10
            game.roll 5
            game.roll 4
            rollMany 16, 0

        then:
            game.score() == 28
    }

    void "Rolling a spare on the last frame calculates the score correctly"() {
        when:
            rollMany 18, 1
            game.roll 5
            game.roll 5
            game.roll 4

        then:
            game.score() == 32
    }

    void "Rolling a strike on the last frame calcualtes the score correctly"() {
        when:
            rollMany 18, 1
            game.roll 10
            game.roll 5
            game.roll 4

        then:
            game.score() == 37
    }

    void "Rolling a perfect game returns a score of 300"() {
        when:
            rollMany 12, 10

        then:
            game.score() == 300
    }

    void rollMany(numTimesToRoll, numPinsKnockedDown) {
        (1..numTimesToRoll).each { game.roll numPinsKnockedDown }
    }
}
