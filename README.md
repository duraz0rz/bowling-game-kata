My implementation of the Bowling Game kata in Groovy. I wanted to deviate from the powerpoint that he presented and use a list of Frame objects instead of depending on indices.

Turned out to be a bit messier than I would have liked, but I can argue it's a bit more understandable than the implementation presented in the powerpoint.

The code's set up as an IntelliJ project, but I also included Gradle.